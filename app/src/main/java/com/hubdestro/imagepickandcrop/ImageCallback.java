package com.hubdestro.imagepickandcrop;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by ankit on 19/9/16
 */
public abstract class ImageCallback {
    public abstract void onSuccess(File imageFile, Bitmap fixBitmap);
    public void onFail() {
    }
}
