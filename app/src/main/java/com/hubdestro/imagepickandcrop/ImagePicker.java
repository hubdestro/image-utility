package com.hubdestro.imagepickandcrop;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ankit on 13/9/16
 */
public class ImagePicker {

    public static final int REQUEST_CAPTURE = 500;
    public static final int REQUEST_GALLERY = 501;
    private static File photoFile;
    private static ImageView selectImage;
    private static Bitmap mBitmap;
    private static Activity activity;
    private static String imagePath;

    /***
     * start intent to pick photo from camera or gallery
     */
    public static void selectImage(final Activity activity) {
        ImagePicker.activity = activity;
        final CharSequence[] options = { "Take a photo", "Choose from gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take a photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                        try {
                            photoFile = createImageFile();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            activity.startActivityForResult(takePictureIntent, REQUEST_CAPTURE);
                        }
                    }
                }
                else if (options[item].equals("Choose from gallery")) {
                    Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    activity.startActivityForResult(intent, REQUEST_GALLERY);

                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /***
     * to handle onActivity data from activity
     */
    public static void handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        //fetch bitmap
        //save into existing directory
        //create path
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_GALLERY:
                    Uri photoUri = data.getData();
                    imagePath = getRealPathFromURI(photoUri);
                    break;
                case REQUEST_CAPTURE:
                    imagePath = photoFile.getAbsolutePath();
                    mBitmap = BitmapFactory.decodeFile(imagePath);
                    break;
            }
        }
    }


    /***
     * to handle onActivity data from activity
     */
    public static void handleOnActivityResult(int requestCode, int resultCode, Intent data, ImageCallback callback) {
        //fetch bitmap
        //save into existing directory
        //create path
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_GALLERY:
                    Uri photoUri = data.getData();
                    if (photoUri != null) {
                        imagePath = getRealPathFromURI(photoUri);
                        photoFile = new File(imagePath);
                        callback.onSuccess(photoFile,getBitmap());
                    }
                    break;
                case REQUEST_CAPTURE:
                    imagePath = photoFile.getAbsolutePath();
                    //photoFile = already handled
                    callback.onSuccess(photoFile,getBitmap());
                    break;
                default:
                    callback.onFail();
            }
        }
    }


    /***
     * to get path of fetch image
     * @return path of bitmap
     */
    public static String getImagePath () {
        return imagePath;
    }

    /***
     * get selected bitmap object
     * @return instance of bitmap
     */
    public static Bitmap getBitmap() {
        mBitmap = BitmapFactory.decodeFile(imagePath);
        mBitmap = changeImageOrientationToOriginal(mBitmap,imagePath);
        return mBitmap;
    }


    private File savebitmap(String filename) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        OutputStream outStream = null;
        File file = new File(filename + ".png");
        if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, filename + ".png");
        }
        try {
            // make a new bitmap from your file
            Bitmap bitmap = BitmapFactory.decodeFile(file.getName());
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    /***
     * to create image file into directory
     * @return file object
     * @throws IOException
     */
    private static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }


    /***
     * to get real path or physical path of image
     * @param uri uri
     * @return path of bitmap
     */
    public static String getRealPathFromURI(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }


    /**
     * Changes the orientation of image to original
     * @param bitmap holds the bitmap
     * @param path holds the image path
     * @return Bitmap
     */
    public static Bitmap changeImageOrientationToOriginal(Bitmap bitmap, String path) {
        ExifInterface ei;
        try {
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return compressResizeImage(bitmap);
    }

    /**
     * Rotates Image
     * @param bitmap holds the bitmap
     * @param degree holds the degree of rotation
     * @return Bitmap
     */
    public static Bitmap rotateImage(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix mtx = new Matrix();
        mtx.postRotate(degree);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }


    /***
     * to compress image
     * @param bm bitmap
     * @return bitmap object
     */
    public static Bitmap compressResizeImage(Bitmap bm) {
        int bmWidth = bm.getWidth();
        int bmHeight = bm.getHeight();
        int ivWidth = 200;
        int ivHeight = 200;
        int new_height = (int) Math.floor((double) bmHeight *( (double) ivWidth / (double) bmWidth));
        Bitmap newbitMap = Bitmap.createScaledBitmap(bm, ivWidth, ivHeight, true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        newbitMap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        Bitmap bm1 = BitmapFactory.decodeByteArray(b, 0, b.length);
        return bm1;
    }


/*    public void openFile(String minmeType) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(minmeType);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", minmeType);
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getPackageManager().resolveActivity(sIntent, 0) != null){
            // it is device with samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
        }
        else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            startActivityForResult(chooserIntent, CHOOSE_FILE_REQUESTCODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }
    }*/

}
